package com.sunzn.puncher.sample;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.sunzn.puncher.library.PuncherView;

public class MainActivity extends AppCompatActivity {

    private final int max = 1000;

    private PuncherView cpv;
    private ImageView status;

    private CheckBox cbTurn;

    private final int[] mShaderColors = new int[]{0xFFFF6F4E, 0xFFFF6F4E, 0xFFFD3666, 0xFFFD3666, 0xFFFF6F4E}; // 可行

    private int progress = 0;

    private final Handler handler = new Handler();

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (progress <= max) {
                cpv.setProgress(progress++);
                handler.postDelayed(this, 5);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        status = findViewById(R.id.status);
        cpv = findViewById(R.id.cpv);
        cbTurn = findViewById(R.id.cbTurn);
        cpv.setProgressColor(mShaderColors);
        cpv.setOnActionListener(new PuncherView.OnActionListener() {
            @Override
            public void onStart() {
                handler.post(runnable);
                status.setImageResource(R.drawable.pause);
            }

            @Override
            public void onPause() {
                cpv.addTick();
                handler.removeCallbacks(runnable);
                status.setImageResource(R.drawable.start);
            }

            @Override
            public void onClose() {
                Toast.makeText(cpv.getContext(), "结束", Toast.LENGTH_SHORT).show();
                handler.removeCallbacks(runnable);
                cpv.addTick();
            }

            @Override
            public void onPrune() {

            }

            @Override
            public void onReset() {
                progress = 0;
                cpv.setProgress(0);
                status.setImageResource(R.drawable.start);
            }

            @Override
            public void onEvent(float progress, float max) {

            }
        });

        cbTurn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                cpv.setTurn(isChecked);
            }
        });

    }


    private void clickBtn0() {
        cpv.cutTick();
        Log.e("WWWWW", "size = " + cpv.getTicks());
    }

    private void clickBtn1() {
        cpv.setProgressColor(mShaderColors);
        cpv.showAnimation(1000, 3000);
    }

    private void clickBtn2() {
        cpv.setProgressColor(mShaderColors);
        cpv.showAnimation(1000, 0, 3000);

    }

    private void clickBtn3() {
        cpv.setProgressColor(0xFF4FEAAC);
        cpv.showAnimation(1000, 3000);
    }

    private void clickBtn4() {
        cpv.setProgressColor(0xFF4FEAAC);
        cpv.showAnimation(1000, 3000);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn0:
                clickBtn0();
                break;
            case R.id.btn1:
                clickBtn1();
                break;
            case R.id.btn2:
                clickBtn2();
                break;
            case R.id.btn3:
                clickBtn3();
                break;
            case R.id.btn4:
                clickBtn4();
                break;
        }
    }
}