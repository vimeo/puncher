package com.sunzn.puncher.library;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class PuncherView extends View implements View.OnClickListener {

    /**
     * 画笔
     */
    private Paint mPaint;
    /**
     * 笔画描边的宽度
     */
    private float mStrokeWidth;

    /**
     * 开始角度(默认从12点钟方向开始)
     */
    private int mStartAngle = 270;
    /**
     * 扫描角度(一个圆)
     */
    private int mSweepAngle = 360;

    /**
     * 圆心坐标x
     */
    private float mCircleCenterX;
    /**
     * 圆心坐标y
     */
    private float mCircleCenterY;

    /**
     * 圆正常颜色
     */
    private int mCircleColor = 0xFFC8C8C8;
    /**
     * 进度颜色
     */
    private int mProgressColor = 0xFF4FEAAC;

    /**
     * 是否使用着色器
     */
    private boolean isShader = true;

    /**
     * 着色器
     */
    private Shader mShader;

    /**
     * 着色器颜色
     */
    private int[] mShaderColors = new int[]{0xFF4FEAAC, 0xFFA8DD51, 0xFFE8D30F, 0xFFA8DD51, 0xFF4FEAAC};

    /**
     * 半径
     */
    private float mRadius;

    /**
     * 刻度的角度大小
     */
    private int mBlockAngle = 1;

    /**
     * 最大进度
     */
    private int mMax = 100;

    /**
     * 当前进度
     */
    private int mProgress = 0;

    /**
     * 动画持续的时间
     */
    private int mDuration = 500;
    /**
     * 进度百分比
     */
    private int mProgressPercent;
    /**
     * 是否旋转
     */
    private boolean isTurn = false;

    /**
     * 是否是圆形线冒（圆角弧度）
     */
    private boolean isCapRound = true;

    private boolean isMeasureCircle = false;

    private OnActionListener mOnActionListener;

    private final List<Integer> mTicks = new ArrayList<>();

    private boolean isProcess = false;

    private RectF mProgressRect;


    public PuncherView(Context context) {
        this(context, null);
    }

    public PuncherView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PuncherView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * 初始化
     *
     * @param context
     * @param attrs
     */
    private void init(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PuncherView);

        DisplayMetrics displayMetrics = getDisplayMetrics();
        mStrokeWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 6, displayMetrics);

        int size = a.getIndexCount();
        for (int i = 0; i < size; i++) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.PuncherView_pv_stroke_width) {
                mStrokeWidth = a.getDimension(attr, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 12, displayMetrics));
            } else if (attr == R.styleable.PuncherView_pv_circle_color) {
                mCircleColor = a.getColor(attr, 0xFFC8C8C8);
            } else if (attr == R.styleable.PuncherView_pv_progress_color) {
                mProgressColor = a.getColor(attr, 0xFF4FEAAC);
                isShader = false;
            } else if (attr == R.styleable.PuncherView_pv_start_angle) {
                mStartAngle = a.getInt(attr, 270);
            } else if (attr == R.styleable.PuncherView_pv_sweep_angle) {
                mSweepAngle = a.getInt(attr, 360);
            } else if (attr == R.styleable.PuncherView_pv_max) {
                mMax = a.getInt(attr, 100);
            } else if (attr == R.styleable.PuncherView_pv_progress) {
                mProgress = a.getInt(attr, 0);
            } else if (attr == R.styleable.PuncherView_pv_duration) {
                mDuration = a.getInt(attr, 500);
            } else if (attr == R.styleable.PuncherView_pv_block_angle) {
                mBlockAngle = a.getInt(attr, mBlockAngle);
            } else if (attr == R.styleable.PuncherView_pv_turn) {
                isTurn = a.getBoolean(attr, isTurn);
            } else if (attr == R.styleable.PuncherView_pv_cap_round) {
                isCapRound = a.getBoolean(attr, isCapRound);
            }
        }

        setOnClickListener(this);

        a.recycle();
        mProgressPercent = (int) (mProgress * 100.0f / mMax);
        mPaint = new Paint();
        mProgressRect = new RectF();
    }

    private DisplayMetrics getDisplayMetrics() {
        return getResources().getDisplayMetrics();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int defaultValue = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getDisplayMetrics());

        int width = measureHandler(widthMeasureSpec, defaultValue);
        int height = measureHandler(heightMeasureSpec, defaultValue);

        // 圆心坐标
        mCircleCenterX = (width + getPaddingLeft() - getPaddingRight()) / 2.0F;
        mCircleCenterY = (height + getPaddingTop() - getPaddingBottom()) / 2.0F;
        //计算间距
        int padding = Math.max(getPaddingLeft() + getPaddingRight(), getPaddingTop() + getPaddingBottom());
        // 半径=视图宽度-横向或纵向内间距值
        mRadius = (width - padding) / 2.0F;

        // 默认着色器
        mShader = new SweepGradient(mCircleCenterX, mCircleCenterX, mShaderColors, null);
        isMeasureCircle = true;

        setMeasuredDimension(width, height);
    }

    /**
     * 测量
     */
    private int measureHandler(int measureSpec, int defaultSize) {
        int result = defaultSize;
        int measureMode = MeasureSpec.getMode(measureSpec);
        int measureSize = MeasureSpec.getSize(measureSpec);
        if (measureMode == MeasureSpec.EXACTLY) {
            result = measureSize;
        } else if (measureMode == MeasureSpec.AT_MOST) {
            result = Math.min(defaultSize, measureSize);
        }
        return result;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawArc(canvas);
    }

    /**
     * 绘制弧形(默认为一个圆)
     */
    private void drawArc(Canvas canvas) {
        mPaint.reset();
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(mStrokeWidth);
        mPaint.setShader(null);
        if (isCapRound) {
            mPaint.setStrokeCap(Paint.Cap.ROUND);
        }

        // 进度圆半径
        float circleRadius = mRadius;
        float diameter = circleRadius * 2;
        float startX = mCircleCenterX - circleRadius;
        float startY = mCircleCenterY - circleRadius;
        float track = mStrokeWidth / 2;
        mProgressRect.set(startX + track, startY + track, startX + diameter - track, startY + diameter - track);

        // 绘制底层圆形
        if (mCircleColor != 0) {
            mPaint.setColor(mCircleColor);
            mPaint.setStyle(Paint.Style.FILL);
            canvas.drawCircle(mCircleCenterX, mCircleCenterY, circleRadius, mPaint);
        }

        // 着色器不为空则设置着色器，反之用纯色
        if (isShader && mShader != null) {
            mPaint.setShader(mShader);
        } else {
            mPaint.setColor(mProgressColor);
        }

        mPaint.setStyle(Paint.Style.STROKE);

        if (isTurn) {
            // 绘制当前进度弧形
            canvas.drawArc(mProgressRect, mStartAngle + mSweepAngle * getRatio(), mSweepAngle * getRatio(), false, mPaint);
        } else {
            // 绘制当前进度弧形
            canvas.drawArc(mProgressRect, mStartAngle, mSweepAngle * getRatio(), false, mPaint);
        }

        // 绘制打点记录
        for (int i = 0; i < mTicks.size(); i++) {
            drawCircle(canvas, mSweepAngle * getRatio(mTicks.get(i)));
        }

    }

    /**
     * 绘制打点记录
     */
    private void drawCircle(Canvas canvas, float deg) {
        float pointer = mRadius - mStrokeWidth / 2;

        float cx, cy;

        if (0 <= deg && deg < 90) {
            cx = (float) (mCircleCenterX + pointer * Math.sin((deg) * Math.PI / 180));
            cy = (float) (mCircleCenterY - pointer * Math.cos((deg) * Math.PI / 180));
        } else if (90 <= deg && deg < 180) {
            cx = (float) (mCircleCenterX + pointer * Math.cos((deg - 90) * Math.PI / 180));
            cy = (float) (mCircleCenterY + pointer * Math.sin((deg - 90) * Math.PI / 180));
        } else if (180 <= deg && deg < 270) {
            cx = (float) (mCircleCenterX - pointer * Math.sin((deg - 180) * Math.PI / 180));
            cy = (float) (mCircleCenterY + pointer * Math.cos((deg - 180) * Math.PI / 180));
        } else {
            cx = (float) (mCircleCenterX - pointer * Math.cos((deg - 270) * Math.PI / 180));
            cy = (float) (mCircleCenterY - pointer * Math.sin((deg - 270) * Math.PI / 180));
        }

        float radius = mStrokeWidth / 3;

        mPaint.reset();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.WHITE);
        mPaint.setStyle(Paint.Style.FILL);

        canvas.drawCircle(cx, cy, radius, mPaint);
    }

    /**
     * 显示进度动画效果（根据当前已有进度开始）
     *
     * @param progress
     */
    public void showAppendAnimation(int progress) {
        showAnimation(mProgress, progress, mDuration);
    }

    /**
     * 显示进度动画效果
     *
     * @param progress
     */
    public void showAnimation(int progress) {
        showAnimation(progress, mDuration);
    }

    /**
     * 显示进度动画效果
     *
     * @param progress
     * @param duration 动画时长
     */
    public void showAnimation(int progress, int duration) {
        showAnimation(0, progress, duration);
    }

    /**
     * 显示进度动画效果，从from到to变化
     *
     * @param from
     * @param to
     * @param duration 动画时长
     */
    public void showAnimation(int from, int to, int duration) {
        showAnimation(from, to, duration, null);
    }

    /**
     * 显示进度动画效果，从from到to变化
     *
     * @param from
     * @param to
     * @param duration 动画时长
     * @param listener
     */
    public void showAnimation(int from, int to, int duration, Animator.AnimatorListener listener) {
        this.mDuration = duration;
        this.mProgress = from;
        ValueAnimator valueAnimator = ValueAnimator.ofInt(from, to);
        valueAnimator.setDuration(duration);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                setProgress((int) animation.getAnimatedValue());
            }
        });

        if (listener != null) {
            valueAnimator.removeAllUpdateListeners();
            valueAnimator.addListener(listener);
        }

        valueAnimator.start();
    }

    /**
     * 进度比例
     *
     * @return
     */
    private float getRatio() {
        return mProgress * 1.0f / mMax;
    }

    private float getRatio(int progress) {
        return progress * 1.0f / mMax;
    }

    /**
     * 设置最大进度
     *
     * @param max
     */
    public void setMax(int max) {
        this.mMax = max;
        invalidate();
    }

    /**
     * 设置当前进度
     *
     * @param progress
     */
    public void setProgress(int progress) {
        this.mProgress = progress;
        mProgressPercent = (int) (mProgress * 100.0f / mMax);
        invalidate();

        if (mOnActionListener != null) {
            mOnActionListener.onEvent(mProgress, mMax);
            if (mProgress >= mMax) {
                isProcess = false;
                mOnActionListener.onClose();
            }
        }
    }

    public void addTick() {
        mTicks.add(mProgress);
        Log.e("WWWWW", "add addTick = " + getTicks());
        invalidate();
    }

    public List<Integer> getTicks() {
        return mTicks;
    }

    public void cutTick() {
        if (isProcess) {
            if (mOnActionListener != null) {
                mOnActionListener.onPause();
                isProcess = !isProcess;
            }
        } else {
            if (mTicks != null && mTicks.size() > 0) {
                mTicks.remove(mTicks.size() - 1);
                setProgress(mTicks.size() > 0 ? mTicks.get(mTicks.size() - 1) : 0);
                invalidate();
                if (mOnActionListener != null) {
                    mOnActionListener.onPrune();
                }
            }
        }
        if (mTicks == null || mTicks.size() == 0) {
            if (mOnActionListener != null) {
                mProgress = 0;
                isProcess = false;
                mOnActionListener.onReset();
            }
        }
    }

    /**
     * 设置正常颜色
     *
     * @param color
     */
    public void setNormalColor(int color) {
        this.mCircleColor = color;
        invalidate();
    }

    /**
     * 设置着色器
     *
     * @param shader
     */
    public void setShader(Shader shader) {
        isShader = true;
        this.mShader = shader;
        invalidate();
    }

    /**
     * 设置进度颜色（通过着色器实现渐变色）
     *
     * @param colors
     */
    public void setProgressColor(int... colors) {
        if (isMeasureCircle) {
            Shader shader = new SweepGradient(mCircleCenterX, mCircleCenterX, colors, null);
            setShader(shader);
        } else {
            mShaderColors = colors;
            isShader = true;
        }
    }

    /**
     * 设置进度颜色（纯色）
     *
     * @param color
     */
    public void setProgressColor(int color) {
        isShader = false;
        this.mProgressColor = color;
        invalidate();
    }

    /**
     * 设置进度颜色
     *
     * @param resId
     */
    public void setProgressColorResource(int resId) {
        int color = getResources().getColor(resId);
        setProgressColor(color);
    }

    /**
     * 设置是否旋转
     *
     * @param isTurn
     */
    public void setTurn(boolean isTurn) {
        this.isTurn = isTurn;
        invalidate();
    }

    /**
     * 是否是圆形线冒（圆角弧度）
     *
     * @param capRound
     */
    public void setCapRound(boolean capRound) {
        isCapRound = capRound;
        invalidate();
    }

    public int getStartAngle() {
        return mStartAngle;
    }

    public int getSweepAngle() {
        return mSweepAngle;
    }

    public float getCircleCenterX() {
        return mCircleCenterX;
    }

    public float getCircleCenterY() {
        return mCircleCenterY;
    }

    public float getRadius() {
        return mRadius;
    }

    public int getMax() {
        return mMax;
    }

    public int getProgress() {
        return mProgress;
    }


    /**
     * 进度百分比
     *
     * @return
     */
    public int getProgressPercent() {
        return mProgressPercent;
    }

    public void setOnActionListener(OnActionListener onActionListener) {
        this.mOnActionListener = onActionListener;
    }

    public interface OnActionListener {

        // 开始
        void onStart();

        // 暂停
        void onPause();

        // 结束
        void onClose();

        // 删除
        void onPrune();

        // 重置
        void onReset();

        // 进度
        void onEvent(float progress, float max);

    }

    public void start() {
        if (mProgress >= mMax) {
            mOnActionListener.onClose();
            return;
        }
        if (!isProcess) {
            isProcess = true;
            mOnActionListener.onStart();
        }
    }

    public void pause() {
        if (isProcess) {
            isProcess = false;
            mOnActionListener.onPause();
        }
    }

    @Override
    public void onClick(View v) {
        if (mProgress >= mMax) {
            mOnActionListener.onClose();
            return;
        }
        isProcess = !isProcess;
        if (isProcess) {
            mOnActionListener.onStart();
        } else {
            mOnActionListener.onPause();
        }
    }

}
